using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using KataAnagrams;
using KataAnagrams.Helper;

namespace KataAnagramsTest
{
    [TestClass]
    public class UnitTest1
    {

        string filledFilePath = @"D:\GitHub\KataAnagrams\KataAnagrams\wordlist.txt";
        string testingFilePath = @"D:\GitHub\KataAnagrams\KataAnagrams\Helper\testingWords.txt";
        string emptyFilePath = @"D:\GitHub\KataAnagrams\KataAnagrams\Helper\empty.txt";
        string noAnagramsPath = @"D:\GitHub\KataAnagrams\KataAnagrams\Helper\NoAnagrams.txt";

        //Aqui probamos el hecho de que si un archivo no existe ValidateFile devuelva false con respecto a su existencia en el archivo marcado.
        [TestMethod]
        public void File_Does_Not_Exist_Returns_False()
        {
            Assert.IsFalse(ValidateFile.FileExist(@"D:\GitHub\KataAnagrams\KataAnagrams\Helper\randomFile.txt"));
        }

        [TestMethod]
        public void File_Exists_Returns_True()
        {
            Assert.IsTrue(ValidateFile.FileExist(filledFilePath));
        }

        //Para esta prueba hay que modificar BiggestWord para que devuelva un entero que la maxima cantidad de anagramas en una palabra.
        [TestMethod]
        public void BiggestAnagram_Returns_13()
        {
            Dictionary<string, List<string>> map = Anagrams.Get(filledFilePath);
            Assert.AreEqual(13, Anagrams.BiggestAnagram(map).Count);
        }

        [TestMethod]
        public void BiggestTestAnagram_Returns_12()
        {
            Dictionary<string, List<string>> map = Anagrams.Get(testingFilePath);
            Assert.AreEqual(12, Anagrams.BiggestAnagram(map).Count);
        }

        [TestMethod]
        public void PrintAnagramsTest_Returns_2()
        {
            Dictionary<string, List<string>> map = Anagrams.Get(testingFilePath);
            Assert.AreEqual(2, Anagrams.PrintAnagrams(map));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Get_Anagrams_From_Empty_File_Throws_Exception()
        {
            Anagrams.Get(emptyFilePath);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void No_Anagrams_Returns_Exception()
        {
            Dictionary<string, List<string>> map = Anagrams.Get(noAnagramsPath);
            Anagrams.PrintAnagrams(map);
        }

        [TestMethod]
        public void Print_Anagrams_Returns_20683()
        {
            Dictionary<string, List<string>> map = Anagrams.Get(filledFilePath);
            Assert.AreEqual(20683, Anagrams.PrintAnagrams(map));
        }


    }
}
