﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KataAnagrams.Helper;

namespace KataAnagrams
{
    public class Anagrams
    {
        public static Dictionary<string,List<string>> Get(string fileName)
        {
            Dictionary<string, List<string>> map = new Dictionary<string, List<string>>();
            
            using (StreamReader sr = new StreamReader(fileName))
            {
                string word;
                while ((word = sr.ReadLine()) != null)
                {
                    char[] letters = word.ToCharArray();
                    Array.Sort(letters);
                    string newWord = new string(letters);

                    if (map.ContainsKey(newWord))
                    {
                        map[newWord].Add(word);
                    }
                    else
                    {
                        List<string> words = new List<string>();
                        words.Add(word);
                        map.Add(newWord, words);
                    }
                }
            }

            if (map.Count == 0)
            {
                throw new Exception();
            }

            return map;
        }

        public static int PrintAnagrams(Dictionary<String, List<String>> map)
        {
            int anagramCount = 0;
            foreach(KeyValuePair<string, List<string>> entry in map)
            {

                if(entry.Value.Count > 1)
                {
                    anagramCount++;
                    Console.WriteLine($"[{string.Join(", ", entry.Value)}]");
                }
            }
            if(anagramCount == 0)
            {
                throw new Exception();
            }
            Console.WriteLine("Total of words: " + map.Count);
            return anagramCount;
        }

        public static List<string> BiggestAnagram(Dictionary<string, List<string>> map)
        {
            int count = 0;
            string key = "";

            foreach (KeyValuePair<string, List<string>> item in map)
            {
                if (item.Value.Count > count)
                {
                    count = item.Value.Count;
                    key = item.Key;
                }
            }

            List<string> words = map[key];

            return words;


        }
    }
}