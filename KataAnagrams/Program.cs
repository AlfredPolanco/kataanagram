﻿using KataAnagrams.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace KataAnagrams
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = @"D:\GitHub\KataAnagrams\KataAnagrams\wordlist.txt";
            bool endApp = false;

            while (!endApp)
            {
                Console.WriteLine("--- Welcome to Kata Anagrams!---\n");
                Console.WriteLine("1. GetAnagramsTime\n" +
                    "2. PrintAnagrams\n" +
                    "3. BiggestAnagram\n" +
                    "4. Exit");
                Console.Write("Select your option: ");
                string option = Console.ReadLine();

                if (ValidateFile.FileExist(fileName))
                {
                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();
                    Dictionary<string, List<string>> map = Anagrams.Get(fileName);
                    stopwatch.Stop();
                    var time = stopwatch.Elapsed.TotalSeconds;
                    var getAnagramstime = time;
                    switch (option)
                    {
                        case "1":
                            Console.WriteLine("Total time in seconds: {0}", getAnagramstime);
                            break;
                        case "2":
                            stopwatch.Start();
                            Anagrams.PrintAnagrams(map);
                            stopwatch.Stop();

                            time = stopwatch.Elapsed.TotalSeconds;

                            Console.WriteLine("Total of time in seconds:" + time);
                            break;
                        case "3":
                            Console.WriteLine(string.Join(",", Anagrams.BiggestAnagram(map)));
                            
                            break;
                        case "4":
                            Console.WriteLine("Exit...");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("File Not Found");
                }

                Console.WriteLine("\n");

                Console.WriteLine("----------------------------------------- \n");

                Console.WriteLine("Press '4' and Enter to close the app, or press any other key and Enter to continue: ");

                if (Console.ReadLine() == "4")
                    endApp = true;

                Console.WriteLine("\n");
            }
            return;
        }
    }
}
